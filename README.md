# R Workshop Summer School 2021

This is the repository for the introductory R Workshop at dbs Summer School 2021

The repository contains an R package. Download it like this:

```
devtools::install_gitlab(“sandra.martin/r-workshop-summer-school-2021“,  
host = “https://gitlab.gwdg.de")
```

